% ----------------------------------------------------------------------------
% function hfssCopy(fid, Names)
% 
% Description :
% -------------
% Creates VB Script necessary to copy HFSS objects.
%
% Parameters :
% ------------
% fid   - file identifier of the HFSS script file.
% Names - a cell array of strings to be copied.
% 
% Note :
% ------
%
% Example :
% ---------
% fid = fopen('myantenna.vbs', 'wt');
% ... 
% hfssCopy(fid, {'Layer1', 'Layer2'});
% ----------------------------------------------------------------------------

function hfssCopy(fid, Names)

fprintf(fid, '\n');
fprintf(fid, 'oEditor.Copy _\n');
fprintf(fid, 'Array("NAME:Selections", _\n');
fprintf(fid, '"Selections:=", _\n');

nObjects = length(Names);
fprintf(fid, '"');
for iO = 1:nObjects-1,
	fprintf(fid, '%s,', Names{iO});
end;
fprintf(fid, '%s")\n', Names{nObjects});
