% ----------------------------------------------------------------------------
% function hfssChangeProperty(fid, Scope, VarName, VarValue, Units)
%
% Description :
% -------------
% Creates the VBScript necessary to set an object's color in HFSS.
%
% Parameters :
% ------------
% fid     - file identifier of the HFSS script file.
% Object  - name of the object whose color requires to be changed.
% Color   - [3x1 vector] represents the [R, G, B] components of the color.
%
% Note :
% ------
%
% Example :
% ---------
% fid = fopen('myantenna.vbs', 'wt');
% ...
% hfssSetColor(fid, 'Substrate', [0, 64, 0]);
%
% ----------------------------------------------------------------------------

function hfssChangeProperty(fid, Scope, PropTab, PropServers, PropType, PropDataArray)

fprintf(fid, '\n');
if strcmp(Scope,'editor')
    fprintf(fid, 'oEditor.changeProperty _\n');
elseif strcmp(Scope,'project')
    fprintf(fid, 'oProject.changeProperty _\n');
elseif strcmp(Scope, 'design')
    fprintf(fid, 'oDesign.changeProperty _\n');
else
    error('Scope is invalid, needs to be ''project'' or ''design''!');
end

fprintf(fid, '\tArray("Name:AllTabs", _\n');
fprintf(fid, ['\t\tArray("Name:' PropTab '", _\n']);
fprintf(fid, ['\t\t\tArray("Name:PropServers", "' PropServers '"), _\n']);
fprintf(fid, ['\t\t\tArray("Name:' PropType '", ' cell2string(PropDataArray) ') _ \n']);
fprintf(fid, '\t\t) _\n');
fprintf(fid, '\t)\n');

end

function [mString] = cell2string(mCellArray)
    mString = 'Array(';
    for i1 = 1:length(mCellArray)
        if iscell(mCellArray{i1})
            mString = [mString ', ' cell2string(mCellArray{i1})];
        else
            if (1 == i1) % Accounts for comma difference between first elementa and else.
                mString = [mString '"' mCellArray{i1} '"'];
            else
                mString = [mString ', "' mCellArray{i1} '"'];
            end
        end
    end
    mString = [mString ')'];
end