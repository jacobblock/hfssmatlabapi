% -------------------------------------------------------------------------- %
% function hfssPolyline(fid, Name, Points)
% Description:
% ------------
%
% Parameters:
% -----------
% Name - Name Attribute for the PolyLine.
% Points - Points as 3-Tuples, ex: Points = [0, 0, 1; 0, 1, 0; 1, 0 1];
%          Note: size(Points) must give [nPoints, 3]
% Units - can be either:
%         'mm' - millimeter.
%         'in' - inches.
%         'mil' - mils.
%         'meter' - meter (note: don't use 'm').
%          or anything that Ansoft HFSS supports.
%
% Example:
% --------
% -------------------------------------------------------------------------- %

function hfssPolyline(fid, Name, Points, Units, Segment, Color, ...
                     Transparency)

if (nargin < 5)
	error('fid, Name, Points, Units, and Segment need to be specified.');
elseif (nargin < 6)
	Color = [];
	Transparency = [];
else
	Transparency = [];
end;

if isempty(Color)
	Color = [0, 0, 0];
end;
if isempty(Transparency)
	Transparency = 0.8;
end;

nPoints = size(Points,2);

% Preamble.
fprintf(fid, '\n');
fprintf(fid, 'oEditor.CreatePolyline _\n');
fprintf(fid, '\tArray("NAME:PolylineParameters", _\n');
fprintf(fid, '\t\t"IsPolylineCovered:=", true, _\n');
fprintf(fid, '\t\t"IsPolylineClosed:=", false, _\n');

% Enter the Points.
fprintf(fid, '\t\tArray("NAME:PolylinePoints", _\n');
for iP = 1:nPoints-1,
	fprintf(fid, '\t\t\tArray("NAME:PLPoint", ');
	fprintf(fid, ['"X:=", "' num2str(Points{iP}{1}) ' ' Units '", ']);
	fprintf(fid, ['"Y:=", "' num2str(Points{iP}{2}) ' ' Units '", ']);
	fprintf(fid, ['"Z:=", "' num2str(Points{iP}{3}) ' ' Units '"), _\n']);
end

fprintf(fid, '\t\t\tArray("NAME:PLPoint", ');
fprintf(fid, ['"X:=", "' num2str(Points{nPoints}{1}) ' ' Units '", ']);
fprintf(fid, ['"Y:=", "' num2str(Points{nPoints}{2}) ' ' Units '", ']);
fprintf(fid, ['"Z:=", "' num2str(Points{nPoints}{3}) ' ' Units '") _\n']);
fprintf(fid, '\t\t), _\n');

% Create Segments.
fprintf(fid, '\t\tArray("NAME:PolylineSegments", _\n');
for i1 = 1:size(Segment,2)-1
    fprintf(fid, '\t\t\tArray("NAME:PLSegment", ');
    fprintf(fid, ['"SegmentType:=",  "' Segment{i1}{1} '", ']);
    fprintf(fid, ['"StartIndex:=", ' num2str(Segment{i1}{2}) ', ']);
    fprintf(fid, ['"NoOfPoints:=", ' num2str(Segment{i1}{3}) '), _\n']);
end
fprintf(fid, '\t\t\tArray("NAME:PLSegment", ');
fprintf(fid, ['"SegmentType:=",  "' Segment{end}{1} '", ']);
fprintf(fid, ['"StartIndex:=",' num2str(Segment{end}{2}) ', ']);
fprintf(fid, ['"NoOfPoints:=", ' num2str(Segment{end}{3}) ') _\n']);
fprintf(fid, '\t\t) _\n');
fprintf(fid, '\t), _\n');

% Polyline Attributes.
fprintf(fid, '\tArray("NAME:Attributes", _\n');
fprintf(fid, '\t\t"Name:=", "%s", _\n', Name);
fprintf(fid, '\t\t"Flags:=", "", _\n');
fprintf(fid, '\t\t"Color:=", "(%d %d %d)", _\n', Color(1), Color(2), Color(3));
fprintf(fid, '\t\t"Transparency:=", %f, _\n', Transparency);
fprintf(fid, '\t\t"PartCoordinateSystem:=", "Global", _\n');
fprintf(fid, '\t\t"MaterialName:=", "vacuum", _\n');
fprintf(fid, '\t\t"SolveInside:=", true _\n');
fprintf(fid, '\t)\n');
