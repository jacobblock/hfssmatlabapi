% -------------------------------------------------------------------------- %
% function hfssHelix(fid, Objects, Center, StartDir, Thread, NumThread, RightHand, Units)
% Description:
% ------------
%
% Parameters:
% -----------
% Name - Name Attribute for the PolyLine.
% Units - can be either:
%         'mm' - millimeter.
%         'in' - inches.
%         'mil' - mils.
%         'meter' - meter (note: don't use 'm').
%          or anything that Ansoft HFSS supports.
%
% Example:
% --------
% -------------------------------------------------------------------------- %

function hfssCreateEquationCurve(fid, name, Xt, Yt, Zt, tStart, tEnd, nPoints)

% Preamble.
fprintf(fid, '\n');
fprintf(fid, 'oEditor.CreateEquationCurve _\n');
fprintf(fid, '\tArray("NAME:EquationBasedCurveParameters", _\n');
fprintf(fid, ['\t\t"XtFunction:=", "' Xt '", _\n']);
fprintf(fid, ['\t\t"YtFunction:=", "' Yt '", _\n']);
fprintf(fid, ['\t\t"ZtFunction:=", "' Zt '", _\n']);
fprintf(fid, ['\t\t"tStart:=", "' num2str(tStart) '", _\n']);
fprintf(fid, ['\t\t"tEnd:=", "' num2str(tEnd) '", _\n']);
fprintf(fid, ['\t\t"NumOfPointsOnCurve:=", "' num2str(nPoints) '" _\n']);
fprintf(fid, '\t), _\n');
fprintf(fid, '\tArray("NAME:Attributes", _\n');
fprintf(fid, ['\t\t"Name:=", "' name '" _\n']);
fprintf(fid, '\t)\n');