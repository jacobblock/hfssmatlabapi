% -------------------------------------------------------------------------- %
% function hfssRenameDesignInstance(fid, oldname, newnname)
%
% Description :
% -------------
% This function creates the VB Script necessary to open a specified HFSS 
% project file in HFSS. The FULL PATH of the HFSS project file must be 
% provided for HFSS to properly open the file (and ofcourse, the file must 
% already exist !)
% 
% Parameters :
% ------------
% fid      - file identifier for the VBScript file.
% hfssFile - the (existing) HFSS project file to be opened. The file 
%            specification must include the FULL FILE PATH and the file 
%            extension.
%
% Note :
% ------
% Use this function if you wish to open an already existing HFSS design and 
% add some MATLAB-generated VB script components into it.
%
% -------------------------------------------------------------------------- %

function hfssRenameDesignInstance(fid, oldname, newname)

fprintf(fid, ['Set oDesign = oProject.SetActiveDesign("' oldname '")\n']);
fprintf(fid, ['oDesign.RenameDesignInstance "' oldname '", "' newname '"\n']);
